# Superfish + Simple Menu Icons - Connector

## What does this module do?
This module adds a compatibility between Superfish and Simple Menu Icons modules.

## Requirements
- Superfish
- Simple Menu Icons

## Installation
Using composer all the dependencies should be automatically installed. Just enabling module and clearing cache afterwards should make all Superfish menus compatible with Simple Menu Icons.

## Configuring
There is no configuration needed.
